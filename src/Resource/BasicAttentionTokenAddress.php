<?php

namespace Coinbase\Wallet\Resource;

use Coinbase\Wallet\Enum\ResourceType;

class BasicAttentionTokenAddress extends Resource
{
    private $address;
    public function __construct($address)
    {
        parent::__construct(ResourceType::BASIC_ATTENTION_TOKEN_ADDRESS);
        $this->address = $address;
    }
    public function getAddress()
    {
        return $this->address;
    }
}