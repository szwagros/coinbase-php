<?php
namespace Coinbase\Wallet\Resource;

use Coinbase\Wallet\Enum\ResourceType;

class BasicAttentionTokenNetwork extends Resource
{
    public function __construct()
    {
        parent::__construct(ResourceType::BASIC_ATTENTION_TOKEN_NETWORK);
    }
}